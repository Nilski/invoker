summon = input("Enter a combination of the three words 'Quas', 'Wex' and 'Exort' to invoke a mighty spell. ")
failure="This spell works nowise! "
spell = "Congratulations, you invoked "

if summon == "Quas Wex Exort" or summon == "Quas Exort Wex" or summon == "Wex Exort Quas"  or summon == "Wex Quas Exort" or summon == "Exort Quas Wex" or summon == "Exort Wex Quas":
    print(spell+"Deafening Blast! This spell unleashes a mighty sonic wave in front of you, dealing damage to any enemy unit it collides based on the level of Exort. The sheer impact from the blast is enough to knock those enemy units back for a duration based on the level of Quas, then disarm their attacks for a duration based on the level of Wex. ")
elif summon == "Quas Quas Wex" or summon == "Quas Wex Quas" or summon == "Wex Quas Quas":
    print(spell+"Ghost Walk! This spell manipulates the ice and electrical energies around you, rendering your body invisible. The elemental imbalance created as a consequence slows nearby enemies based on the level of Quas, and slows you as well based on the level of Wex. ")
elif summon == "Wex Wex Quas" or summon == "Wex Quas Wex" or summon == "Quas Wex Wex":
    print(spell+"Tornado! Unleashes a fast moving tornado that picks up enemy units in its path, suspending them helplessly in the air shortly before allowing them to plummet to their doom. Travels further based on the level of Wex. Holds enemies in the air for a duration based on the level of Quas. Deals damage based on the levels of Quas and Wex. ")
elif "Quas Quas Quas" in summon:
    print(spell+"Cold Snap! You draw the heat from an enemy, chilling them to their very core for a duration based on Quas. Further damage taken in this state will freeze the enemy again, dealing bonus damage. The enemy can only be frozen so often, but the freeze cooldown decreases based on the level of Quas. ")
elif "Exort Exort Exort" in summon:
    print(spell+"Sun Strike! Sends a catastrophic ray of fierce energy from the sun at any targeted location, incinerating all enemies standing beneath it once it reaches the Earth. Deals damage based on the level of Exort, however this damage is spread evenly over all enemies hit. ")
elif "Wex Wex Wex" in summon:
    print(spell+"EMP! You build up a charge of electromagnetic energy at a targeted location which automatically detonates after a duration based on the level of Wex. The detonation covers an area, draining mana based on the level of Wex. Deals damage for each point of mana drained. ")
elif summon == "Quas Quas Exort" or summon == "Quas Exort Quas" or summon == "Exort Quas Quas":
    print(spell+"Ice Wall! Generates a wall of solid ice directly in front of you for a duration based on the level of Quas. The bitter cold emanating from it greatly slows nearby enemies based on the level of Quas and deals damage each second based on the level of Exort. ")
elif summon == "Exort Exort Quas" or summon == "Exort Quas Exort" or summon == "Quas Exort Exort":
    print(spell+"Forge Spirits!  You forge a spirit embodying the strength of fire and fortitude of ice. Damage, health, and armor are based on the level of Exort while attack range, mana, and duration are based on the level of Quas. The elemental's scorching attack is capable of melting the armor of enemies. If both Quas and Exort are level 4 or higher, you will create two spirits instead of one. ")
elif summon == "Exort Exort Wex" or summon == "Exort Wex Exort" or summon == "Wex Exort Exort":
    print(spell+"You pull a flaming meteor from space onto the targeted location. Upon landing, the meteor rolls forward, constantly dealing damage based on the level of Exort, and rolling further based on the level of Wex. Units hit by the meteor will also be set on fire for a short time, receiving additional damage based on the level of Exort. ")
elif summon == "Exort Wex Wex" or summon == "Wex Exort Wex" or summon == "Wex Wex Exort":
    print(spell+"Alacrity! You infuse an ally with an immense surge of energy, increasing their attack speed based on the level of Wex and their damage based on the level of Exort.")
else:
    print(failure)
